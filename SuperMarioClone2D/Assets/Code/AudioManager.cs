﻿using UnityEngine.Audio;
using System;
using UnityEngine;

public class AudioManager : MonoBehaviour {
    public static AudioManager Instance;
    public Sound[] sounds;

    void Awake() {
        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }
        if (Instance)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        Play("MainMenuBGM");
        
        
    }

    void Start()
    {

    }
    public void PitchChange(string name, float pitchValue)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        //if (s == null)
        //    return;
        //s.pitch += pitchValue;
        if (s.source != null)
            s.source.pitch += pitchValue;

    }

    public void Play(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
            return;
        if (s.source != null)
            s.source.Play();
    }
    public void Stop(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
            return;
        if(s.source != null)
            s.source.Stop();
    }
}

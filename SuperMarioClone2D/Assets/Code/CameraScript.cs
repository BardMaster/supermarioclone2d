﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    public static CameraScript Instance;
    Transform PlayerPosition;
    //private Transform PlayerTransform;
    //public GameObject[] Player;
    //public Transform[] PlayerTransforms;
    private Vector3 _cameraoffset;
    private Vector3 velocity = Vector3.zero;
    [Range(0.1f, 10f)]
    public float smoothness;

    void Awake()
    {
        //if (Instance)
        //    DestroyImmediate(gameObject);
        //else
        //{
        //    DontDestroyOnLoad(gameObject);
        //    Instance = this;
        //}

        if (GameObject.Find("Player") != null)
        {
            PlayerPosition = GameObject.Find("Player").transform;
        }
        else if (GameObject.Find("Player2") != null)
        {
            PlayerPosition = GameObject.Find("Player2").transform;
        }
        else if (GameObject.Find("Player3") != null)
        {
            PlayerPosition = GameObject.Find("Player3").transform;
        }

        //this.transform.position = PlayerPosition - new Vector3(0,0,10);
        _cameraoffset = transform.position - PlayerPosition.transform.position;
        if (FindObjectOfType<AudioManager>() != null)
        { 
        FindObjectOfType<AudioManager>().Stop("MainMenuBGM");
        FindObjectOfType<AudioManager>().Play("BGM");
        }
    }

    private void FixedUpdate()
    {


        Vector3 newPos = PlayerPosition.transform.position + _cameraoffset;

        transform.position = Vector3.SmoothDamp(transform.position, newPos, ref velocity, smoothness);

    }
}


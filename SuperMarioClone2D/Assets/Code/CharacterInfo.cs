﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInfo : MonoBehaviour {
    //public CharacterSelection Charselect;
    //public static CharacterInfo Instance;
    public GameObject PlayerOne;
    public GameObject PlayerOneLife;
    public GameObject PlayerTwo;
    public GameObject PlayerTwoLife;
    public GameObject PlayerThree;
    public GameObject PlayerThreeLife;
    public GameObject Camera;
    void Start () {
        // GameObject.DontDestroyOnLoad(gameObject);
        

        if (CharacterSelection.characterSelected == 1)
        {
            PlayerOne.SetActive(true);
            PlayerOneLife.SetActive(true);
            Camera.SetActive(true);
        }
        else if (CharacterSelection.characterSelected == 2)
        {
            PlayerTwo.SetActive(true);
            PlayerTwoLife.SetActive(true);
            Camera.SetActive(true);
        }
        else if (CharacterSelection.characterSelected == 3)
        {
            PlayerThree.SetActive(true);
            PlayerThreeLife.SetActive(true);
            Camera.SetActive(true);
        }
    }

    private void Awake()
    {
        //if (Instance)
        //    DestroyImmediate(gameObject);
        //else
        //{
        //    DontDestroyOnLoad(gameObject);
        //    Instance = this;
        //}
    }

    // Update is called once per frame
    void Update () {
		
	}
}

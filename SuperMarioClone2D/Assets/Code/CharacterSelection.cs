﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterSelection : MonoBehaviour {

    public static int characterSelected = 1;
    public static int characterScore = 0;
    public static int characterLives = 5;
    public static int characterGems = 0;
    public static int characterScoreCache = 0;
	void Start () {
        GameObject.DontDestroyOnLoad(gameObject);
    }



    public void PlayerOne()
    {
        characterSelected = 1;
        LoadLevelOne();
    }
    public void PlayerTwo()
    {
        characterSelected = 2;
        LoadLevelOne();
    }
    public void PlayerThree()
    {
        characterSelected = 3;
        LoadLevelOne();
    }

    private void LoadLevelOne()
    {
        SceneManager.LoadScene("Level1");
    }
}

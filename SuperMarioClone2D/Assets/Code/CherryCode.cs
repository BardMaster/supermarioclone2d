﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CherryCode : MonoBehaviour {

    [SerializeField] Transform DeathZone;
    [SerializeField] Transform CherrySpawn;
    //public CherryController CherryControl;
    public bool setCherryToSpawn = false;

    void Start () {
		
	}
	
	void Update () {
		if(setCherryToSpawn == true)
        {
            transform.position = CherrySpawn.transform.position;
            setCherryToSpawn = false;
        }
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == (int)Layer.Player)
        {
            transform.position = DeathZone.transform.position;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CherryController : MonoBehaviour {
    public static CherryController Instance;
    public CherryCode Cherry1;
    public CherryCode Cherry2;
    public CherryCode Cherry3;
    public bool cherryReset = false;

	void Awake () {
        //if (Instance)
        //    DestroyImmediate(gameObject);
        //else
        //{
        //    DontDestroyOnLoad(gameObject);
        //    Instance = this;
        //}
    }
	
	void Update () {
		if(cherryReset == true)
        {
            if (Cherry1 != null)
            {
                Cherry1.setCherryToSpawn = true;
            }
            if (Cherry2 != null)
            {
                Cherry2.setCherryToSpawn = true;
            }
            if (Cherry3 != null)
            {
                Cherry3.setCherryToSpawn = true;
            }
            cherryReset = false;
        }
	}
}

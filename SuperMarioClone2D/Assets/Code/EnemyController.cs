﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    //public static EnemyController Instance;
    public EnemyMoveScript EnemyCode0;
    public EnemyMoveScript EnemyCode1;
    public EnemyMoveScript EnemyCode2;
    public EnemyMoveScript EnemyCode3;
    public EnemyMoveScript EnemyCode4;
    public EnemyMoveScript EnemyCode5;
    public EnemyMoveScript EnemyCode6;
    public EnemyMoveScript EnemyCode7;
    public EnemyMoveScript EnemyCode8;

    private void Awake()
    {
        //if (Instance)
        //    DestroyImmediate(gameObject);
        //else
        //{
        //    DontDestroyOnLoad(gameObject);
        //    Instance = this;
        //}
    }
    public void ResetEnemies()
    {
        if (EnemyCode0!=null)
        EnemyCode0.EnemyReset();
        if (EnemyCode1!=null)
        EnemyCode1.EnemyReset();
        if (EnemyCode2!=null)
        EnemyCode2.EnemyReset();
        if (EnemyCode3!=null)
        EnemyCode3.EnemyReset();
        if (EnemyCode4!=null)
        EnemyCode4.EnemyReset();
        if (EnemyCode5!=null)
        EnemyCode5.EnemyReset();
        if (EnemyCode6!=null)
        EnemyCode6.EnemyReset();
        if (EnemyCode7!=null)
        EnemyCode7.EnemyReset();
        if (EnemyCode8!=null)
        EnemyCode8.EnemyReset();


    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMoveScript : MonoBehaviour {

    [SerializeField] private float moveSpeed;
    [SerializeField] private GameObject deathZone;
    [SerializeField] private GameObject EnemySpawnPoint;
    [SerializeField] private Animator EnemyAnimator;
    public Player PlayerStats;
    public Player Player2Stats;
    public bool enemyFacingRight;
    private bool isDead = false;
    public float deathTimer = 0f;


    private void Awake ()
    {
        transform.localScale -= new Vector3(2f, 0, 0); //CHECKED
        enemyFacingRight = true;
    }
	



    public void EnemyReset()
    {
        transform.position = EnemySpawnPoint.transform.position;
    }

    private void FixedUpdate() //this could be its own function, like move and then get called in the Slug scripts
    {
        if (!isDead)
        transform.Translate(new Vector3(moveSpeed, 0, 0) * Time.deltaTime);

        if (deathTimer > 0)
        {
            deathTimer -= Time.fixedDeltaTime;
            if (deathTimer < 0)
            {
                deathTimer = 0;
            }
        }
        if (deathTimer == 1)
            transform.position = deathZone.transform.position;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    { // checking if the enemy is facing right and also making him walk between set walls.
        if (collision.gameObject.layer == (int)Layer.EnemyWall && enemyFacingRight == true)
        {
            transform.Rotate(new Vector3(0, 180, 0));
            enemyFacingRight = false;
        }
        else if (collision.gameObject.layer == (int)Layer.EnemyWall && enemyFacingRight == false)
        {
            transform.Rotate(new Vector3(0, 180, 0));
            enemyFacingRight = true;
        }
    }

    public IEnumerator myCoroutine()
    {
        EnemyAnimator.SetTrigger("EnemyDeathTrigger");
        isDead = true;
        yield return new WaitForSeconds(1);
        isDead = false;
        EnemyDie();
    }

    public void EnemyDie()
    {
        //EnemyAnimator.SetTrigger("EnemyDeathTrigger");

        transform.position = deathZone.transform.position;

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinishStarCode : MonoBehaviour {
    private int nextLevelNo = 3;
    private Rigidbody2D myRigidBody;
    private Collider2D starCollider;
    
    [SerializeField] private float StarSpeed;
    private void Awake()
    {
        myRigidBody = GetComponent<Rigidbody2D>();
        starCollider = GetComponent<Collider2D>();
        Scene currentScene = SceneManager.GetActiveScene();
    }
    //public void StarCaught()
    //{
    //    myRigidBody.AddForce(new Vector3(0, StarSpeed, 0));
    //    starCollider.enabled = false;
    //}
    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    if (collision.gameObject.layer == (int)Layer.Player)
    //    {
    //        StartCoroutine(LoadNextLevel(waitSeconds));
    //    }
    //}

    public IEnumerator LoadNextLevel(int waitSeconds)
    {
        myRigidBody.AddForce(new Vector3(0, StarSpeed, 0));
        starCollider.enabled = false;
        FindObjectOfType<AudioManager>().Stop("BGM");
        FindObjectOfType<AudioManager>().Play("EndLevelMusic");
        yield return new WaitForSeconds(waitSeconds);
        CharacterSelection.characterScoreCache = CharacterSelection.characterScore;
        SceneManager.LoadScene(nextLevelNo);
        
        
    }
}

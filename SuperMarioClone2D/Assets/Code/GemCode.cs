﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GemCode : MonoBehaviour {
    [SerializeField] Transform DeathZone;
    [SerializeField] Transform GemSpawn;
    public GemControllerCode2 GemControl;
    public bool setGemToSpawn = false;



    private void Start()
    {
    }

    void Update () {
        if (setGemToSpawn == true)
        {
            transform.position = GemSpawn.transform.position;
            setGemToSpawn = false;
        }
    }

    public void GemPickup()
    {
                transform.position = DeathZone.transform.position;

    }
    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    if (collision.gameObject.layer == (int)Layer.Player)
    //    {
    //        GemControl.gemTotal += 1;
    //        transform.position = DeathZone.transform.position;
    //    }
    //}
}

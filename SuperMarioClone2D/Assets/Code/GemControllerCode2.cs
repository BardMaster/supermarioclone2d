﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GemControllerCode2 : MonoBehaviour {
    public GemControllerCode2 Instance;
    public GemCode Gemcode;
    public GemCode Gemcode1;
    public GemCode Gemcode2;
    public GemCode Gemcode3;
    public GemCode Gemcode4;
    public GemCode Gemcode5;
    public GemCode Gemcode6;
    public GemCode Gemcode7;
    public GemCode Gemcode8;
    public bool GemReset = false;
    //public int gemTotal = CharacterSelection.characterGems;
    void Start()
    {
        //gemTotal = CharacterSelection.characterGems;
    }
    private void Awake()
    {
        //if (Instance)
        //    DestroyImmediate(gameObject);
        //else
        //{
        //    DontDestroyOnLoad(gameObject);
        //    Instance = this;
        //}
    }

    void Update () {
        
        if (GemReset == true)
        {   if (Gemcode != null)
            {
                Gemcode.setGemToSpawn = true;
            }
            if (Gemcode1 != null)
            {
                Gemcode1.setGemToSpawn = true;
            }
            if (Gemcode2 != null)
            {
                Gemcode2.setGemToSpawn = true;
            }
            if (Gemcode3 != null)
            {
                Gemcode3.setGemToSpawn = true;
            }
            if (Gemcode4 != null)
            {
                Gemcode4.setGemToSpawn = true;
            }
            if (Gemcode5 != null)
            {
                Gemcode5.setGemToSpawn = true;
            }
            if (Gemcode6 != null)
            {
                Gemcode6.setGemToSpawn = true;
            }
            if (Gemcode7 != null)
            {
                Gemcode7.setGemToSpawn = true;
            }
            if (Gemcode8 != null)
            {
                Gemcode8.setGemToSpawn = true;
            }
            CharacterSelection.characterGems = 0;
            GemReset = false;
            
        }
    }
}

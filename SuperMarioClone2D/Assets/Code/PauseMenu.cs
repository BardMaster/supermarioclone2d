﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
public class PauseMenu : MonoBehaviour {
    [SerializeField] private TMP_Text levelText;
    public static PauseMenu Instance;
    public bool GameIsPaused = false;
    public GameObject PauseMenuUI;

    private void Awake()
    {
        PauseMenuUI.SetActive(false);
        StartCoroutine(ShowLevelName(3));   
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown("escape"))
        {
            if (GameIsPaused)
            {
                Resume();
            } else
            {
                Pause();
            }
        }
    }

    public void Resume()
    {
        PauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
        Cursor.visible = false;
    }

    void Pause()
    {
        PauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
        Cursor.visible = true;
    }

    public void LoadMenu()
    {
        Time.timeScale = 1f;
        CharacterSelection.characterScore = 0;
        CharacterSelection.characterGems = 0;
        CharacterSelection.characterLives = 5;
        SceneManager.LoadScene("MainMenu");
        FindObjectOfType<AudioManager>().Stop("BGM");
        FindObjectOfType<AudioManager>().Play("MainMenuBGM");
    }

    public void QuitGame()
    {
        Debug.Log("Quitting game...");
        Application.Quit();
    }
    
    public void RestartGame()
    {
        Time.timeScale = 1f;
        Debug.Log("Restarting game...");
        Scene currentScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(currentScene.name);

    }
    public void CharacterSelect()
    {
        Time.timeScale = 1f;
        Debug.Log("SELECT YOUR CHARACTER");
        SceneManager.LoadScene("CharacterSelect");

    }

    public IEnumerator ShowLevelName(int waitTime)
    {
        yield return new WaitForSeconds(0.5f);
        levelText.gameObject.SetActive(true);
        yield return new WaitForSeconds(waitTime);
        levelText.gameObject.SetActive(false);

    }
}


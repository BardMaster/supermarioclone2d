﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Events;


public class Player : MonoBehaviour
{
    //Delegate :D
    private delegate void InputDelegate();
    InputDelegate move;
    //Dictionary
    private Dictionary<KeyCode, InputDelegate> input;

    public int health;
    public int gems;
    [SerializeField] private float jumpAngle;
    [SerializeField] private GameObject spawnPoint;
    [SerializeField] private Text Lives;
    [SerializeField] private Text timeText;
    [SerializeField] private Text Score;
    [SerializeField] private Text Gems;
    [SerializeField] private Text Health;
    [SerializeField] private GameObject heart1;
    [SerializeField] private GameObject heart2;
    [SerializeField] private GameObject heart3;
    [SerializeField] private GameObject heart4;
    [SerializeField] private GameObject heart5;
    [SerializeField] private GameObject PowerCarrot1;
    [SerializeField] private Animator animator;
    public bool EnemyHitFacingRight = false;
    public bool EnemyHitFacingLeft = false;
    [SerializeField] float timeLeft = 300.0f;
    private Rigidbody2D myRigidBody;
    [SerializeField] private int bounceForce = 1500;
    [SerializeField] private int knockBackForce = 200;
    public GemControllerCode2 GemController;
    public EnemyController myEnemyController;
    public CherryController myCherryController;
    private float hitTimer = 0f;
    public float powerupTimer = 0f;
    [SerializeField] private float m_JumpForce = 400f;                          // Amount of force added when the player jumps.
    [Range(0, 1)] [SerializeField] private float m_CrouchSpeed = .36f;          // Amount of maxSpeed applied to crouching movement. 1 = 100%
    [Range(0, .3f)] [SerializeField] private float m_MovementSmoothing = .05f;  // How much to smooth out the movement
    [SerializeField] private bool m_AirControl = false;                         // Whether or not a player can steer while jumping;
    [SerializeField] private LayerMask m_WhatIsGround;                          // A mask determining what is ground to the character
    [SerializeField] private Transform m_GroundCheck;                           // A position marking where to check if the player is grounded.
    [SerializeField] private Transform m_CeilingCheck;                          // A position marking where to check for ceilings
    [SerializeField] private Collider2D m_CrouchDisableCollider;                // A collider that will be disabled when crouching
    public float runSpeed = 40f;
    float horizontalMove = 0f;
    bool jump = false;
    bool crouch = false;

    const float k_GroundedRadius = .2f; // Radius of the overlap circle to determine if grounded
    [SerializeField] private bool m_Grounded;            // Whether or not the player is grounded.
    const float k_CeilingRadius = .2f; // Radius of the overlap circle to determine if the player can stand up
    private Rigidbody2D m_Rigidbody2D;
    private bool m_FacingRight = true;  // For determining which way the player is currently facing.
    private Vector3 m_Velocity = Vector3.zero;

    [Header("Events")]
    [Space]

    [SerializeField] private UnityEvent OnLandEvent;

    [System.Serializable]
    [SerializeField] private class BoolEvent : UnityEvent<bool> { }

    [SerializeField] private BoolEvent OnCrouchEvent;
    private bool m_wasCrouching = false;

    void Start()
    {
        input = new Dictionary<KeyCode, InputDelegate>();
        InGameControls(); // DONT FORGET THIS CRAP OK

        Cursor.visible = false;
        gems = CharacterSelection.characterGems;
        health = 50;        //setting health
        Lives.text = CharacterSelection.characterLives.ToString();
        Score.text = CharacterSelection.characterScore.ToString();
        Health.text = health.ToString();
    }

    private void Awake()
    {
        myRigidBody = GetComponent<Rigidbody2D>();
        m_Rigidbody2D = GetComponent<Rigidbody2D>();

        if (OnLandEvent == null)
            OnLandEvent = new UnityEvent();

        if (OnCrouchEvent == null)
            OnCrouchEvent = new BoolEvent();
    }

    void InGameControls()
    {
        input = new Dictionary<KeyCode, InputDelegate>()
        {
            {KeyCode.A,() => {CallMoveMethod(); } },
            {KeyCode.D,() => {CallMoveMethod(); } },
            {KeyCode.S,() => {CallMoveMethod(); } },
            {KeyCode.W,() => {CallMoveMethodWithJump(); } },


        };

        //input.Add(KeyCode.A, null);
        //input.Add(KeyCode.D, null);
        //input[KeyCode.A] = CallMoveMethod;
        //input[KeyCode.D] = CallMoveMethod;
    }

    void CallMoveMethod()
    {
        Move(horizontalMove * Time.fixedDeltaTime, crouch, jump);
    }

    void CallMoveMethodWithJump()
    {
        jump = true;
        Move(horizontalMove * Time.fixedDeltaTime, crouch, jump);
        jump = false;
    }

    private void OnCollisionEnter2D(Collision2D collision) // check for when player hits anything
    {
        if (collision.gameObject.layer == (int)Layer.Spikes)
        {
            health -= 10;
            Bounce();
            FindObjectOfType<AudioManager>().Play("EnemyHit");
        }
        EnemyMoveScript enemy = collision.collider.GetComponent<EnemyMoveScript>();


        if (enemy != null && hitTimer == 0)
        {
            foreach (ContactPoint2D point in collision.contacts)
            {

                Vector2 mypos = transform.position;
                Vector2 otherpos = point.point;
                float adj = otherpos.y - mypos.y;
                float opp = otherpos.x - mypos.x;

                float tan = Mathf.Atan(opp / adj) * Mathf.Rad2Deg;



                Debug.Log("tan= " + tan);

                if (tan >= -jumpAngle && tan <= jumpAngle)
                {
                    Debug.Log("enemyhurt");
                    StartCoroutine(enemy.myCoroutine());
                    Bounce();
                    ScorePlus(100);
                    FindObjectOfType<AudioManager>().Play("EnemyBounce");
                    break;

                }
                else// if (enemy.enemyFacingRight)
                {
                    hitTimer = 1;
                    Hit(enemy.enemyFacingRight);
                    break;

                }

            }

        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == (int)Layer.Gem)
        {
            GemCode gem = collision.GetComponent<GemCode>();
            CharacterSelection.characterGems += 1;
            ScorePlus(100);
            gem.GemPickup();
            FindObjectOfType<AudioManager>().Play("GemCollect");
        }

        if (collision.gameObject.layer == (int)Layer.Carrot)
        {
            ScorePlus(1000);
            PowerCarrot1.SetActive(false);
            PowerUp();

        }

        if (collision.gameObject.layer == (int)Layer.Star)
        {
            FinishStarCode star = collision.GetComponent<FinishStarCode>();
            CharacterSelection.characterScore += (int)timeLeft * 10;
            FindObjectOfType<AudioManager>().Play("GemCollect");
            if (star != null)
                StartCoroutine(star.LoadNextLevel(4));
        }

        if (collision.gameObject.layer == (int)Layer.Cherry && health == 50)
        {
            ScorePlus(100);
            FindObjectOfType<AudioManager>().Play("CherrySound");
        }
        else if (collision.gameObject.layer == (int)Layer.Cherry)
        {
            health += 10;
            FindObjectOfType<AudioManager>().Play("CherrySound");
        }
    }

    private void PowerUp()
    {
        runSpeed += 40;
        hitTimer = 10;
        powerupTimer = 10;
        FindObjectOfType<AudioManager>().PitchChange("BGM", 0.5f);
    }

    private void PowerDown()
    {

        runSpeed -= 40;
        FindObjectOfType<AudioManager>().PitchChange("BGM", -0.5f);
    }

    private void Bounce()
    {
        myRigidBody.AddForce(new Vector2(0f, bounceForce));
    }

    private void ScorePlus(int scoreValue)
    {
        CharacterSelection.characterScore += scoreValue;
    }

    void Update()
    {
        if (hitTimer > 0)
        {
            hitTimer -= Time.deltaTime;
            if (hitTimer < 0)
            {
                hitTimer = 0;
            }
        }
        if (powerupTimer > 0)
        {
            powerupTimer -= Time.deltaTime;
            if (powerupTimer < 0)
            {
                powerupTimer = 0;
                PowerDown();
            }
        }

        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;

        animator.SetFloat("Speed", Mathf.Abs(horizontalMove));

        if (Input.GetButtonDown("Jump")) //Making him Jump
        {
            jump = true;
            animator.SetBool("IsJumping", true);

        }

        if (Input.GetButtonDown("Crouch")) //Making him Crouch 
        {
            crouch = true;
        }
        else if (Input.GetButtonUp("Crouch"))
        {
            crouch = false;
        }

        Lives.text = CharacterSelection.characterLives.ToString();
        Score.text = CharacterSelection.characterScore.ToString();
        Gems.text = CharacterSelection.characterGems.ToString();
        Health.text = health.ToString();
        if (health >= 50)
        {
            health = 50;
        }
        if (health == 50)
        {
            heart5.SetActive(true);
        }
        else if (health == 40)
        {
            heart5.SetActive(false);
            heart4.SetActive(true);
        }
        else if (health == 30)
        {
            heart4.SetActive(false);
            heart3.SetActive(true);
        }
        else if (health == 20)
        {
            heart3.SetActive(false);
            heart2.SetActive(true);
        }
        else if (health == 10)
        {
            heart2.SetActive(false);
            heart1.SetActive(true);
        }

        if (gameObject.transform.position.y < -10) // if player falls below the map he gets killed.
        {
            Die();
        }
        if (health <= 0) // if health reaches zero, he gets killed
        {
            Die();
        }
        timeLeft -= Time.deltaTime;
        timeText.text = "~" + Mathf.Round(timeLeft);
        if (timeLeft <= 0)
        {
            Die();
        }

        if (CharacterSelection.characterLives == 0) //if lives = 0 go to game over screen
        {
            OutOfLives();
        }
    }

    private void FixedUpdate() // update called every set amount of frames, predetermined
    {
        bool wasGrounded = m_Grounded;
        m_Grounded = false;

        // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
        // This can be done using layers instead but Sample Assets will not overwrite your project settings.
        Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
            {
                m_Grounded = true;
                if (!wasGrounded && m_Rigidbody2D.velocity.y < 0)
                    OnLandEvent.Invoke();
            }
        }

        //Move our character
        ////////////////////////////Move(horizontalMove * Time.fixedDeltaTime, crouch, jump);
        jump = false;

        if (Input.GetKey(KeyCode.A))
        {
            input[KeyCode.A].Invoke();
        }
        else if (Input.GetKey(KeyCode.D))
        {
            input[KeyCode.D].Invoke();
        }
        if (Input.GetKey(KeyCode.W))
        {
            input[KeyCode.W].Invoke();
        }
        else if (Input.GetKey(KeyCode.S))
        {
            input[KeyCode.S].Invoke();

        }
    }

    private void Move(float move, bool crouch, bool jump)
    {
        // If crouching, check to see if the character can stand up
        if (!crouch)
        {
            // If the character has a ceiling preventing them from standing up, keep them crouching
            if (Physics2D.OverlapCircle(m_CeilingCheck.position, k_CeilingRadius, m_WhatIsGround))
            {
                crouch = true;
            }
        }

        //only control the player if grounded or airControl is turned on
        if (m_Grounded || m_AirControl)
        {

            // If crouching
            if (crouch)
            {
                if (!m_wasCrouching)
                {
                    m_wasCrouching = true;
                    animator.SetBool("IsCrouching", true);
                    //OnCrouchEvent.Invoke(true);
                }

                // Reduce the speed by the crouchSpeed multiplier
                move *= m_CrouchSpeed;

                // Disable one of the colliders when crouching
                if (m_CrouchDisableCollider != null)
                    m_CrouchDisableCollider.enabled = false;
            }
            else
            {
                // Enable the collider when not crouching
                if (m_CrouchDisableCollider != null)
                    m_CrouchDisableCollider.enabled = true;

                if (m_wasCrouching)
                {
                    m_wasCrouching = false;
                    //OnCrouchEvent.Invoke(false);
                    animator.SetBool("IsCrouching", false);

                }
            }

            // Move the character by finding the target velocity
            Vector3 targetVelocity = new Vector2(move * 10f, m_Rigidbody2D.velocity.y);
            // And then smoothing it out and applying it to the character
            m_Rigidbody2D.velocity = Vector3.SmoothDamp(m_Rigidbody2D.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);

            // If the input is moving the player right and the player is facing left...
            if (move > 0 && !m_FacingRight)
            {
                // ... flip the player.
                Flip();
            }
            // Otherwise if the input is moving the player left and the player is facing right...
            else if (move < 0 && m_FacingRight)
            {
                // ... flip the player.
                Flip();
            }
        }
        // If the player should jump...
        if (m_Grounded && jump)
        {
            // Add a vertical force to the player.
            m_Grounded = false;
            m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));
            FindObjectOfType<AudioManager>().Play("PlayerJump");
        }
    }

    public void OnLanding()
    {
        animator.SetBool("IsJumping", false);
    }

    public void OnCrouching(bool isCrouching)
    {
        animator.SetBool("IsCrouching", true);
    }

    private void Flip()
    {
        // Switch the way the player is labelled as facing.
        m_FacingRight = !m_FacingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    public void Hit(bool facingRight)
    {
        health -= 10;
        animator.SetTrigger("TookDamageTrigger");
        if (facingRight)
            myRigidBody.AddForce(new Vector3(knockBackForce, 200, 0));
        else
            myRigidBody.AddForce(new Vector3(-knockBackForce, 200, 0));
        FindObjectOfType<AudioManager>().Play("EnemyHit");
    }

    public void Die()
    {
        CharacterSelection.characterLives += -1;
        CharacterSelection.characterScore = CharacterSelection.characterScoreCache;
        this.transform.position = spawnPoint.transform.position;
        health = 50;
        timeLeft = 300.0f;
        hitTimer = 1;
        myRigidBody.velocity = Vector3.zero;
        myRigidBody.Sleep();
        FindObjectOfType<AudioManager>().Play("PlayerDeath");
        myEnemyController.ResetEnemies();
        myCherryController.cherryReset = true;
        GemController.GemReset = true;
        heart2.SetActive(true);
        heart3.SetActive(true);
        heart4.SetActive(true);
        heart5.SetActive(true);
        if (PowerCarrot1 != null)
            PowerCarrot1.SetActive(true);


        if (powerupTimer > 0)
        {
            PowerDown();
            powerupTimer = -1;
        }
    }

    private void OutOfLives()
    {
        CharacterSelection.characterLives = 5;
        SceneManager.LoadScene("DeadScreen");
        Cursor.visible = true;
        FindObjectOfType<AudioManager>().Stop("BGM");
    }
}

public enum Layer
{
    Default = 0,
    Player = 8,
    EnemyWall = 12,
    Enemy = 13,
    Gem = 14,
    Cherry = 15,
    Spikes = 16,
    Star = 17,
    Carrot = 18
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Retry : MonoBehaviour {

    public void LevelOne ()
    {
        SceneManager.LoadScene("Level1");
    }
    public void QuitToMain ()
    {
        SceneManager.LoadScene("MainMenu");
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour {
    [SerializeField] private GameObject Door1;
    [SerializeField] private GameObject Player1;
    [SerializeField] private GameObject Player2;
    [SerializeField] private GameObject Player3;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == (int)Layer.Player && Input.GetButtonDown("Action"))
        {
            Debug.Log("MOVE");
            if (Player1 != null)
                Player1.transform.position = Door1.transform.position;
            else if (Player2 != null)
                Player2.transform.position = Door1.transform.position;
            else if (Player3 != null)
                Player3.transform.position = Door1.transform.position;
        }
    }
}
